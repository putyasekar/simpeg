<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="description" content="Mobile Application HTML5 Template">

  <meta name="copyright" content="MACode ID, https://www.macodeid.com/">

  <title>PENDAFTARAN CALON KARYAWAN</title>

  <link rel="shortcut icon" href="../assets/favicon.png" type="image/x-icon">

  <link rel="stylesheet" href="../assets/css/maicons.css">

  <link rel="stylesheet" href="../assets/vendor/animate/animate.css">

  <link rel="stylesheet" href="../assets/vendor/owl-carousel/css/owl.carousel.min.css">

  <link rel="stylesheet" href="../assets/css/bootstrap.css">

  <link rel="stylesheet" href="../assets/css/mobster.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark navbar-floating">
  <div class="container">
    <a class="navbar-brand" href="index.html">
      <img src="../assets/favicon-light.png" alt="" width="40">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarToggler">
      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
        <!-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index.html">Homepage 1</a>
            <a class="dropdown-item" href="index-2.html">Homepage 2</a>
          </div>
        </li> -->
        <!-- <li class="nav-item">
          <a class="nav-link" href="about.html">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="blog.html">Blog</a>
        </li>
       -->
        <!-- <li class="nav-item active">
          <a class="nav-link" href="contact.html">Daftar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="updates.html">Table</a>
        </li>
      </ul> -->
      <div class="ml-auto my-2 my-lg-0">
        <button class="btn btn-dark rounded-pill">Isi Form Berikut</button>
      </div>
    </div>
  </div>
</nav>

<div class="bg-light">

  <div class="page-hero-section bg-image hero-mini" style="background-image: url(../assets/img/hero_mini.svg);">
    <div class="hero-caption">
      <div class="container fg-white h-100">
        <div class="row justify-content-center align-items-center text-center h-100">
          <div class="col-lg-6">
            <h3 class="mb-2">FORMULIR PENDAFTARAN CALON KARYAWAN</h3>
            <!-- <nav aria-label="breadcrumb">
              <ol class="breadcrumb breadcrumb-dark justify-content-center bg-transparent">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact</li>
              </ol>
            </nav> -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="page-section">
    <div class="container">
      <!-- <div class="row justify-content-center">
        <div class="col-lg-10 my-3 wow fadeInUp">
          <div class="card-page">
            <div class="row row-beam-md">
              <div class="col-md-4 text-center py-3 py-md-2">
                <i class="mai-location-outline h3"></i>
                <p class="fg-primary fw-medium fs-vlarge">Location</p>
                <p class="mb-0">3 East Ridgewood Avenue Los Angeles, CA 90022</p>
              </div>
              <div class="col-md-4 text-center py-3 py-md-2">
                <i class="mai-call-outline h3"></i>
                <p class="fg-primary fw-medium fs-vlarge">Contact</p>
                <p class="mb-1">+213 908 92034</p>
                <p class="mb-0">+415 123 89245</p>
              </div>
              <div class="col-md-4 text-center py-3 py-md-2">
                <i class="mai-mail-open-outline h3"></i>
                <p class="fg-primary fw-medium fs-vlarge">Email</p>
                <p class="mb-1">support@mobster.com</p>
                <p class="mb-0">support@macodeid.com</p>
              </div>
            </div>
          </div>
        </div> -->

        <form role="form" action="/daftar" method="POST" enctype="multipart/form-data">
                    @csrf
                      <div class="box-body">

                      {{-- get session flash --}}
                        @if(Session::has('message'))
                        <h4><strong>{{session::get('message')}}</strong></h4>
                      @endif

                      {{-- get validation --}}
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                        <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                        </div>
                      @endif
        
        <div class="col-md-18 col-lg-18 my-18 wow fadeInUp">
          <div class="card-page">
           <!-- start: RESPONSIVE TABLE PANEL -->
           <div class="panel panel-default">
                            <!-- <div class="panel-heading">
                                <i class="fa fa-file-text"></i>
                                Data yang harus Anda lengkapi
                            </div> -->
            <div class="panel-body">                                
            <form role="form" enctype="multipart/form-data" id="form-job-hire" action="/index.php?r=Site/ApplyJobHire" method="post">
            <div style="display:none"><input type="hidden" value="1b571c446a731bea9c620a7433e717960b7b2124" name="YII_CSRF_TOKEN" /></div>                                <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                    <label class="control-label">
                    Nama Lengkap<span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="" placeholder="Nama Lengkap" maxlength="100" minlength="2" style="text-transform:uppercase" required="required">
                </div>
                <div class="col-lg-6">
                    <label class="control-label">
                    Email <span class="symbol required"></span>
                    </label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="EMAIL" maxlength="100" minlength="2" required="required">
                </div>
                </div>